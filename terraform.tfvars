# terraform variables values, update this file with your own values

heroku_pipeline_name = "heroku-terraform"

heroku_develop_app = "heroku-terraform-develop"

heroku_staging_app = "heroku-terraform-staging"

heroku_production_app = "heroku-terraform-production"

heroku_region = "eu"

heroku_app_buildpacks = ["https://github.com/mars/create-react-app-buildpack.git"]

