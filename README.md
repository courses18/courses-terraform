# courses-terraform

## Heroku

Platform as a service 


## Besoin 

Dans le cadre du déploiement de sa nouvelle applicaton, le projet a besoin de trois environnements.
L'environnement de développement, de recette et de production.

Ta mission si tu l'acceptes est de créer de manière automatisée ces éléments.

## Terraform

Terraform est un outil d'orchestration (Infras as code)

- Go et [HCL](https://www.terraform.io/docs/language/index.html)
- [Liste des providers](https://registry.terraform.io/browse/providers)

- [Installation de terraform](https://www.terraform.io/downloads.html)

- Si vous êtes derrière un proxy:

export HTTP_PROXY="http://<URL>:<PORT>"
export HTTPS_PROXY="http://<URL>:<PORT>"


export HEROKU_API_KEY=<TOKEN> HEROKU_EMAIL=<EMAIL>

- Vérifier la version

```sh
terraform version
```

- créer notre projet

```sh
mkdir main.tf variables.tf apps.tf pipeline.tf
```

- Langage HCL

- Préciser le provider

```hcl
provider "heroku" {
  email   = "${var.heroku_account_email}"
  api_key = "${var.heroku_api_key}"
}
```

- Notion de variables
```hcl
variable "nom_de_ma_variable" {
    description = "A quoi sert ma variable"
    type = "type de ma variable (string, list, map....)"
    default = "valeur par défaut si une n'est pas renseigné"
}
```

Dans notre cas
```hcl
variable "heroku_account_email" {}
variable "heroku_api_key" {}
```


- chargement du plugin 
```sh
terraform init
```

- création des pipelines
```hcl
resource "nom_de_ma_resource" "identifiant_de_ma_resource" {
  # listes d'arguments
}
```

Ajouter le code suivant 
```hcl
resource "heroku_pipeline" "pipeline" {
  name = "${var.heroku_pipeline_name}"
}
```


N'oublie de mettre à jour le fichier des variables
```hcl
variable "heroku_pipeline_name" {}
```
```sh
terraform plan
```

- création des groupes d'applications

# Heroku apps
```hcl
resource "heroku_app" "develop" {
  name   = "${var.heroku_develop_app}"
  region = "${var.heroku_region}"

  config_vars = {
    APP_ENV = "develop"
  }

  buildpacks = "${var.heroku_app_buildpacks}"
}

resource "heroku_app" "staging" {
  name   = "${var.heroku_staging_app}"
  region = "${var.heroku_region}"

  config_vars = {
    APP_ENV = "staging"
  }

  buildpacks = "${var.heroku_app_buildpacks}"
}

resource "heroku_app" "production" {
  name   = "${var.heroku_production_app}"
  region = "${var.heroku_region}"

  config_vars = {
    APP_ENV = "production"
  }

  buildpacks = "${var.heroku_app_buildpacks}"
}
```

- Mettre à jour le fichier de variables
```hcl

variable "heroku_develop_app" {}
variable "heroku_staging_app" {}
variable "heroku_production_app" {}
variable "heroku_region" {}

variable "heroku_app_buildpacks" {
  type = "list"
}
```

```sh
terraform plan
```


- il est temps de créer les bases 
```hcl
resource "heroku_addon" "nom_de_nom_addon" {
  app  = ""
  plan = ""
}
```

```hcl
# Develop database
resource "heroku_addon" "develop" {
  app  = "${heroku_app.develop.name}"
  plan = "${var.heroku_develop_database}"
}

# Staging database
resource "heroku_addon" "staging" {
  app  = "${heroku_app.staging.name}"
  plan = "${var.heroku_staging_database}"
}

# Production database
resource "heroku_addon" "production" {
  app  = "${heroku_app.production.name}"
  plan = "${var.heroku_production_database}"
}
```

- et comme d'habitude n'oublie pas de mettre à jour le fichier de variables

```hcl
variable "heroku_develop_database" {}
variable "heroku_staging_database" {}
variable "heroku_production_database" {}
```


- lier les groupes d'app au pipeline

```hcl
resource "heroku_pipeline_coupling" "develop" {
  app      = "${heroku_app.develop.name}"
  pipeline = "${heroku_pipeline.pipeline.id}"
  stage    = "development"
}

resource "heroku_pipeline_coupling" "staging" {
  app      = "${heroku_app.staging.name}"
  pipeline = "${heroku_pipeline.pipeline.id}"
  stage    = "staging"
}

resource "heroku_pipeline_coupling" "production" {
  app      = "${heroku_app.production.name}"
  pipeline = "${heroku_pipeline.pipeline.id}"
  stage    = "production"
}
```

- créer le fichier de variables à réellement utilisé
```hcl
# terraform variables values, update this file with your own values
heroku_account_email = "john.doe@example.com"

heroku_api_key = "heroku_api_key"

heroku_pipeline_name = "heroku-terraform"

heroku_develop_app = "heroku-terraform-develop"

heroku_staging_app = "heroku-terraform-staging"

heroku_production_app = "heroku-terraform-production"

heroku_region = "us"

heroku_develop_database = "heroku-postgresql:hobby-basic"
heroku_staging_database = "heroku-postgresql:hobby-basic"
heroku_production_database = "heroku-postgresql:hobby-basic"

heroku_app_buildpacks = [
  "heroku/go",
]
```

```sh
terraform plan
terraform apply
```

# [Terraform cloud](https://app.terraform.io)
