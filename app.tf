resource "heroku_app" "develop" {
  name   = "${var.heroku_develop_app}"
  region = "${var.heroku_region}"

  config_vars = {
    APP_ENV = "develop"
  }

 #buildpacks = "${var.heroku_app_buildpacks}"
}

resource "heroku_app" "staging" {
  name   = "${var.heroku_staging_app}"
  region = "${var.heroku_region}"

  config_vars = {
    APP_ENV = "staging"
  }

 #buildpacks = "${var.heroku_app_buildpacks}"
}

resource "heroku_app" "production" {
  name   = "${var.heroku_production_app}"
  region = "${var.heroku_region}"

  config_vars = {
    APP_ENV = "production"
  }


 #buildpacks = "${var.heroku_app_buildpacks}"
}

# Build code & release to the app
resource "heroku_build" "develop_build" {
  app        = "${heroku_app.develop.name}"
  buildpacks = ["https://github.com/mars/create-react-app-buildpack.git"]

  source {
    url     = "https://github.com/mars/cra-example-app/archive/v2.1.1.tar.gz"
    version = "2.1.1"
  }
}


resource "heroku_build" "staging_build" {
  app        = "${heroku_app.staging.name}"
  buildpacks = ["https://github.com/mars/create-react-app-buildpack.git"]

  source {
    url     = "https://github.com/mars/cra-example-app/archive/v2.1.1.tar.gz"
    version = "2.1.1"
  }
}


resource "heroku_build" "production_build" {
  app        = "${heroku_app.production.name}"
  buildpacks = ["https://github.com/mars/create-react-app-buildpack.git"]

  source {
    url     = "https://github.com/mars/cra-example-app/archive/v2.1.1.tar.gz"
    version = "2.1.1"
  }
}

output "develop_app_url" {
  value = "https://${heroku_app.develop.name}.herokuapp.com"
}

output "production_app_url" {
  value = "https://${heroku_app.production.name}.herokuapp.com"
}

output "staging_app_url" {
  value = "https://${heroku_app.staging.name}.herokuapp.com"
}
